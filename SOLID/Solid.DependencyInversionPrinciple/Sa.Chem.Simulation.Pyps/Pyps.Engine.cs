﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Sa.Chem
{
	public sealed partial class Pyps : Simulation
	{
		private const string simModelId = "PYPS";

		private sealed class Constants
		{
			internal const string FileUsrSol = "USRSOL.EXE";
			internal const string FileSolInp = "SOLINP.DAT";
			internal const string FileMdlOut = "MDLOUT.DAT";
			internal const string FileNapIn = "NAPIN.DAT";
			internal const string FileCoeYld = "COEYLD.DAT";
			internal const string FileTemp = "TEMP1.DAT";

			internal const string OpCondMs25 = "MS25";
			internal const string MdlOutError = "**********";

			//	Zero Based list; these values should be one (1) less than the column number
			internal const int IndexNapInN = 10;
			internal const int IndexNapInG = 11;
			internal const int IndexNapIn1 = 12;
			internal const int IndexNapIn2 = 13;

			internal const int IndexIbpInN = 81;
			internal const int IndexIbpInG = 94;
			internal const int IndexIbpIn1 = 103;
			internal const int IndexIbpIn2 = 117;

			internal const int IndexSgInN = 80;
			internal const int IndexSgInG = 93;
			internal const int IndexSgIn1 = 102;
			internal const int IndexSgIn2 = 116;

			internal const int ItemsSolInp = 158;
			internal const int ItemsMdlOut = 25;

			internal const int PypsTimeOut = 150;
		}

		private sealed class PypsErrorCode
		{
			internal const int SolInpCreation = -530;
			internal const int SolInpError = -530;

			internal const int MdlOutCreation = -530;
			internal const int MdlOutError = -530;

			internal const int NapInVerify = -530;
			internal const int NapInReset = -530;

			internal const int ExecutePyps = -530;

			/// <summary>
			/// USRSOL.EXE exited improperly.
			/// </summary>
			/// <remarks>
			/// USRSOL.EXE returns the the exit code <c>129</c> when failing.
			/// </remarks>
			internal const int UsrSolExit = 129;

			/// <summary>
			/// USRSOL.EXE enountered a <c>divide by zero</c> operation.
			/// </summary>
			/// <remarks>
			/// USRSOL.EXE returns the exit code <c>131</c> when a <c>divide by zero</c> error occurs.
			/// </remarks>
			internal const int UsrSolDivZ = 131;
		}

		internal sealed class Engine : AEngine
		{
			public Engine(string pathEngine = "Engine.Pyps", string pathIsolated = "Engine.Temp", bool autoDelete = false)
				: base(pathEngine, pathIsolated, autoDelete)
			{
			}

			//TODO:	Verify VerifyEngine
			protected override bool VerifyEngine()
			{
				return true;
			}

			//TODO:	Verify ResetEngine
			protected override void ResetEngine()
			{
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileSolInp);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileMdlOut);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileTemp);

				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileNapIn);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileCoeYld);

				Sa.FileOps.Copyfile(this.PathEngine + Constants.FileCoeYld, this.PathIsolated + Constants.FileCoeYld, true);
				Sa.FileOps.Copyfile(this.PathEngine + Constants.FileNapIn, this.PathIsolated + Constants.FileNapIn, true);
			}

			public override void LoopFeeds(SqlCommand cmd)
			{
				string factorSetId;
				string refnum;
				string opCondId;
				string streamId;
				string streamDescription;
				int recycleId;

				cmd.Connection.Open();

				using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
				{
					while (rdr.Read())
					{
						factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
						refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));
						streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));
						streamDescription = rdr.GetString(rdr.GetOrdinal("StreamDescription"));
						opCondId = rdr.GetString(rdr.GetOrdinal("OpCondId"));
						recycleId = rdr.GetInt32(rdr.GetOrdinal("RecycleId"));

						Console.WriteLine("\t" + "\t" + refnum + "\t" + streamId + "\t" + opCondId);

						//SimulateFeed(rdr);

						IInputReader iRdr = new SolInpDatabaseReader(rdr);
						IOutputWriter oWtr = new MdlOutDatabaseWriter();

						Conditions conditions = new Conditions(simModelId, factorSetId, refnum, opCondId, streamId, streamDescription, recycleId);

						SimulateFeed(iRdr, oWtr, conditions);
					}
				}
				cmd.Connection.Close();
			}

			public override void SimulateFeed(IInputReader iRdr, IOutputWriter oWtr, Conditions conditions)
			{
				SolInput solInput = new SolInput(iRdr.Read().InputItems);

				bool UploadResults = false;

				if (iRdr.Verified())
				{
					IInputWriter iWtr = new SolInpWriter(this.PathIsolated + Constants.FileSolInp);
					iWtr.Write(solInput);

					if (iWtr.Exists())
					{
						this.Execute();

						IOutputReader oRdr = new MdlOutReader(this.PathIsolated + Constants.FileMdlOut);

						if (oRdr.Exists() && oRdr.Verified())
						{
							SimResults simResults = oRdr.Read(conditions);

							if (solInput.IsLiquid)
							{
								if (!VerifyNapIn(solInput))
								{
									if (ResetNapId(solInput))
									{
										UploadResults = true;
									}
								}
								else
								{
									UploadResults = true;
								}
							}
							else
							{
								UploadResults = true;
							}

							if (UploadResults) oWtr.Write(conditions, simResults);
						}
					}
				}
			}

			private List<string> ReadNapIn()
			{
				string napin = File.ReadAllText(this.PathIsolated + Constants.FileNapIn, Encoding.ASCII);
				napin = Regex.Replace(napin, @"\s+", Environment.NewLine);

				return (!string.IsNullOrEmpty(napin)) ? new List<string>(napin.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)) : new List<string>();
			}

			private bool VerifyNapIn(SolInput solInput)
			{
				int e = 0;

				List<string> NapIn = ReadNapIn();

				for (int i = 5; i <= 11; i++)
				{
					if (Single.Parse(NapIn[i], NumberFormatInfo.InvariantInfo) == solInput.InputItems[i + solInput.IndexIbp - 5].FValue)
					{
						e++;
					}
				}

				return (e == 7);
			}

			private bool ResetNapId(SolInput solInput)
			{
				SolInput tSolInput = new SolInput(solInput.InputItems);

				//	Increase the specific gravity
				tSolInput.InputItems[solInput.IndexDensity].FValue = solInput.Density + 0.1F;

				//	Reduce the initial boiling point
				tSolInput.InputItems[solInput.IndexIbp].FValue = solInput.InitialBoilingPoint - 5F;

				//	Reset all files
				ResetEngine();

				//	Run simulation with temporary values
				IInputWriter iWtr = new SolInpWriter(this.PathIsolated + Constants.FileSolInp);
				iWtr.Write(tSolInput);
				this.Execute();

				//	Delete SolInp and MdlOut files
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileSolInp);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileMdlOut);

				//	Run simulation with original values
				iWtr.Write(solInput);
				this.Execute();

				//	Verify NapIn
				return this.VerifyNapIn(solInput);
			}

			private sealed class SolInput : SimInput, ILiquidProperties
			{
				private float val = 1f;

				public SolInput(InputItems inputItems)
					: base(inputItems)
				{
				}

				public bool IsLiquid
				{
					get
					{

						if (base.InputItems[Constants.IndexNapInN].FValue == val) return true;
						if (base.InputItems[Constants.IndexNapInG].FValue == val) return true;
						if (base.InputItems[Constants.IndexNapIn1].FValue == val) return true;
						if (base.InputItems[Constants.IndexNapIn2].FValue == val) return true;

						return false;
					}
				}

				public int IndexIbp
				{
					get
					{
						if (base.InputItems[Constants.IndexNapInN].FValue == val) return Constants.IndexIbpInN;
						if (base.InputItems[Constants.IndexNapInG].FValue == val) return Constants.IndexIbpInG;
						if (base.InputItems[Constants.IndexNapIn1].FValue == val) return Constants.IndexIbpIn1;
						if (base.InputItems[Constants.IndexNapIn2].FValue == val) return Constants.IndexIbpIn2;

						return 0;
					}
				}

				public int IndexDensity
				{
					get
					{
						if (base.InputItems[Constants.IndexNapInN].FValue == val) return Constants.IndexSgInN;
						if (base.InputItems[Constants.IndexNapInG].FValue == val) return Constants.IndexSgInG;
						if (base.InputItems[Constants.IndexNapIn1].FValue == val) return Constants.IndexSgIn1;
						if (base.InputItems[Constants.IndexNapIn2].FValue == val) return Constants.IndexSgIn2;

						return 0;
					}
				}

				public float InitialBoilingPoint
				{
					get
					{
						if (this.IndexIbp > 0)
						{
							return base.InputItems[this.IndexIbp].FValue;
						}
						return 0;
					}
				}

				public float Density
				{
					get
					{
						if (this.IndexDensity > 0)
						{
							return base.InputItems[this.IndexDensity].FValue;
						}
						return 0;
					}
				}
			}

			private abstract class InputReader : IInputReader
			{
				protected internal InputItems inputItems = new InputItems();

				public bool Verified()
				{
					return (this.inputItems.Count == Constants.ItemsSolInp);
				}

				public abstract SimInput Read();
			}

			private sealed class SolInpDatabaseReader : InputReader
			{
				private readonly SqlDataReader Rdr;

				internal SolInpDatabaseReader(SqlDataReader readerRow)
				{
					this.Rdr = readerRow;
				}

				public override SimInput Read()
				{
					string col;
					string val;

					int ord;

					for (int idx = 1; idx <= Constants.ItemsSolInp; idx++)
					{
						col = idx.ToString(NumberFormatInfo.InvariantInfo);

						if (this.Rdr.FieldExists(col))
						{
							ord = this.Rdr.GetOrdinal(col);
							val = this.GetValue(this.Rdr, ord);

							val = (!string.IsNullOrEmpty(val)) ? val : "0";

							base.inputItems.Add(new InputItem(col, float.Parse(val)));
						}
						else
						{
							base.inputItems.Add(new InputItem(col, 0f));
						}
					}
					return new SolInput(base.inputItems);
				}

				private string GetValue(SqlDataReader rdr, int ord)
				{
					switch (rdr.GetDataTypeName(ord))
					{
						case "float":
							return rdr.GetSqlDouble(ord).ToString();
						case "int":
							return rdr.GetSqlInt32(ord).ToString();
						case "real":
							return rdr.GetSqlSingle(ord).ToString();
						case "decimal":
							return rdr.GetSqlDecimal(ord).ToString();
						default:
							return rdr.GetSqlValue(ord).ToString();
					}
				}
			}

			private sealed class SolInpReader : InputReader
			{
				private readonly string SolInpPath;

				internal SolInpReader(string pathInput)
				{
					this.SolInpPath = pathInput;
				}

				public override SimInput Read()
				{
					string[] delimiter = new string[] { " " };

					string solInp = System.IO.File.ReadAllText(this.SolInpPath, Encoding.ASCII);

					List<string> lst = new List<string>(solInp.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries));

					string col;
					float val;

					foreach (string s in lst)
					{
						col = lst.IndexOf(s).ToString();
						val = float.Parse(s);

						base.inputItems.Add(new InputItem(col, val));
					}
					return new SolInput(base.inputItems);
				}
			}

			private sealed class SolInpWriter : IInputWriter
			{
				private readonly string SolInpPath;

				internal SolInpWriter(string path)
				{
					this.SolInpPath = path;
				}

				public void Write(SimInput simInput)
				{
					string str = string.Empty;

					foreach (InputItem item in simInput.InputItems)
					{
						str = str + " " + item.FValue + " ";
					}

					File.WriteAllText(this.SolInpPath, str, Encoding.ASCII);
				}

				public bool Exists()
				{
					return (File.Exists(SolInpPath));
				}
			}

			private sealed class MdlOutReader : IOutputReader
			{
				private readonly string mdlOutPath;

				private List<string> mdlOut;

				internal MdlOutReader(string mdlOutPath)
				{
					this.mdlOutPath = mdlOutPath;
				}

				public bool Exists()
				{
					return File.Exists(this.mdlOutPath);
				}

				public bool Verified()
				{
					this.mdlOut = this.ReadMdlOut();
					return (this.mdlOut.Count == Constants.ItemsMdlOut);
				}

				private List<string> ReadMdlOut()
				{
					string output = File.ReadAllText(this.mdlOutPath, Encoding.ASCII);

					return (!string.IsNullOrEmpty(output))
						? new List<string>(output.Split(Environment.NewLine.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries))
						: new List<string>();
				}

				public SimResults Read(Conditions conditions)
				{
					Yield yield = ReadYield(conditions.opCondId, this.mdlOut);
					Energy energy = ReadEnergy(conditions.opCondId, this.mdlOut);

					return new SimResults(yield, energy);
				}

				private Yield ReadYield(string opCondId, List<string> mdlOut)
				{
					float fCompWt = float.NaN;
					string sCompId;

					Yield yield = new Yield();

					for (int i = 4; i <= mdlOut.Count - 3; i++)
					{
						sCompId = ConvertComponentId(mdlOut[i].Substring(1, 14).Trim());
						fCompWt = ParseMdlOutLine(opCondId, mdlOut[i]);

						yield.Add(new Component(sCompId, fCompWt));
					}

					return yield;
				}

				private Energy ReadEnergy(string opCondId, List<string> mdlOut)
				{
					float fEnergy = float.NaN;

					fEnergy = ParseMdlOutLine(opCondId, mdlOut[24]);

					return new Energy(fEnergy);
				}

				private static float ParseMdlOutLine(string opCondId, string mdlOutLine)
				{
					string sOutput = null;
					float fOutput = float.NaN;

					if (opCondId != Constants.OpCondMs25)
					{
						sOutput = mdlOutLine.Substring(21, 12).Trim();
					}
					else
					{
						sOutput = mdlOutLine.Substring(31, mdlOutLine.Length - 31).Trim();
					}

					if (sOutput != Constants.MdlOutError && IsNumeric(sOutput))
					{
						fOutput = float.Parse(sOutput, System.Globalization.NumberFormatInfo.InvariantInfo);
					}

					return fOutput;
				}

				/// <summary>
				/// Converts the MDLOUT.DAT composition names to the Solomon ComponentID (dim.CompositionLu)
				/// </summary>
				/// <param name="pypsComponent">MDLOUT.DAT compsition name.</param>
				/// <returns>String</returns>
				/// <remarks>
				/// C9-400F and FUEL OIL transformation defined by Calcs!YIRCalc (Yield Prediction)
				/// </remarks>
				private static string ConvertComponentId(string pypsComponent)
				{
					string ComponentId = string.Empty;

					switch (pypsComponent)
					{
						case "HYDROGEN":
							ComponentId = "H2";
							break;
						case "METHANE":
							ComponentId = "CH4";
							break;
						case "ACETYLENE":
							ComponentId = "C2H2";
							break;
						case "ETHYLENE":
							ComponentId = "C2H4";
							break;
						case "ETHANE":
							ComponentId = "C2H6";
							break;
						case "MA + PD":
							ComponentId = "C3H4";
							break;
						case "PROPYLENE":
							ComponentId = "C3H6";
							break;
						case "PROPANE":
							ComponentId = "C3H8";
							break;
						case "BUTADIENE":
							ComponentId = "C4H6";
							break;
						case "BUTENE":
							ComponentId = "C4H8";
							break;
						case "BUTANE":
							ComponentId = "C4H10";
							break;
						case "C5-S":
							ComponentId = "C5S";
							break;
						case "C6-C8 NA":
							ComponentId = "C6C8NA";
							break;
						case "BENZENE":
							ComponentId = "C6H6";
							break;
						case "TOLUENE":
							ComponentId = "C7H8";
							break;
						case "XY + ETB":
							ComponentId = "C8H10";
							break;
						case "STYRENE":
							ComponentId = "C8H8";
							break;
						case "C9-400F":
							ComponentId = "PyroGasOil";
							break;
						case "FUEL OIL":
							ComponentId = "PyroFuelOil";
							break;
						default:
							ComponentId = string.Empty;
							break;
					}
					return ComponentId;
				}

				private static bool IsNumeric(string value)
				{
					float v;
					return (float.TryParse(value, out v));
				}
			}

			private sealed class MdlOutDatabaseWriter : IOutputWriter
			{
				public void Write(Conditions conditions, SimResults simResults)
				{
					WriteEnergy(conditions, simResults.energy);
					WriteYield(conditions, simResults.yield);
				}

				private void WriteEnergy(Conditions conditions, Energy energy)
				{
					float fEnergy = energy.amount;
					fEnergy = (!float.IsNaN(fEnergy)) ? fEnergy : -1f;

					Database.InsertEnergy(conditions.factorSetId, conditions.refnum, conditions.simModelId, conditions.opCondId, conditions.streamId, conditions.streamDescription, conditions.recycleId, "", fEnergy);
				}

				private void WriteYield(Conditions conditions, Yield yield)
				{
					string cString = Sa.Database.dbConnectionString("DBS5", "Olefins", "", "");

					using (SqlConnection cn = new SqlConnection(cString))
					{
						foreach(Component comp in yield)
						{
						Database.InsertComposition(cn, conditions.factorSetId, conditions.refnum, conditions.simModelId, conditions.opCondId, conditions.streamId, conditions.streamDescription, conditions.recycleId, "", comp.id, comp.wtPcnt);
						}
					}
				}
			}

			protected override int Execute()
			{
				int rtn = ErrorCode.ExecuteSimulation;

				using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
				{
					proc.EnableRaisingEvents = true;

					proc.StartInfo.CreateNoWindow = true;
					proc.StartInfo.FileName = this.PathIsolated + Constants.FileUsrSol;
					proc.StartInfo.UseShellExecute = false;
					proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
					proc.StartInfo.WorkingDirectory = this.PathIsolated;

					rtn = Execute(proc, Constants.PypsTimeOut);

					// Re-run if PYPS improperly exits.
					// Occasionally, PYPS will improperly exit but will run normally when executed a second time.
					if (rtn == PypsErrorCode.UsrSolExit)
					{
						Execute(proc, Constants.PypsTimeOut);
					}
				}

				return rtn;
			}

			/// <summary>
			/// Executes USRSOL.EXE and verifies is exit code.
			/// </summary>
			/// <param name="proc">Defined process object for USRSOL.EXE</param>
			/// <param name="timeOut">Number of milliseconds to pass before exiting USRSOL.EXE.</param>
			/// <returns>System.Int32</returns>
			/// <remarks>Starts USRSOL.EXE and waits for execution to complete.
			/// If execution does not complete, the process is killed removing file locks.</remarks>
			private static int Execute(System.Diagnostics.Process proc, System.Int32 timeOut)
			{
				int rtn = PypsErrorCode.ExecutePyps;

				try
				{
					proc.Start();
					proc.WaitForExit(timeOut);

					if (proc.HasExited)
					{
						rtn = proc.ExitCode;
					}
					else
					{
						try
						{
							proc.Kill();
						}
						catch (InvalidOperationException ex)
						{
							rtn = ErrorCode.ExecuteKill;
							Console.WriteLine(ex);
							//Sa.Log.Write("Could not kill " & proc.ProcessName & " (" & proc.Id & ") process", EventLogEntryType.Error, False, Sa.Log.LogName, 0)
						}
					}
					proc.Close();
					GC.Collect();
				}
				catch (System.ComponentModel.Win32Exception ex)
				{
					rtn = ErrorCode.ExecuteWinEx;
					Console.WriteLine(ex);
				}

				return rtn;
			}
		}
	}
}